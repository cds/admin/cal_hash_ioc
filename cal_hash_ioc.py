#!/bin/env python3
# managed by puppet
from typing import Dict, Any, Optional, Callable
import argparse
import json
import sys
import os

from ligo_softioc import SoftIOC

class Channel(object):
    def __init__(self, name: str,  chan_def: Dict[str, Any], convert_type: Callable=int, default=0, old_save_name: Optional[str]=None):
        """
        :param name:
        :param save_name: name to use in JSON file, if None just use channel name
        :param chan_def: pcas py channel definition dictionary
        """
        self.name = name
        self.old_save_name = old_save_name
        self.chan_def = chan_def
        self.value: Any = default
        self.convert_type = convert_type

    def get_save_name(self) -> str:
        if self.old_save_name is None:
            return self.name
        else:
            return self.old_save_name

channels = [
    Channel("ID_INT", {'type': 'int'}, int, 0, 'cal_id'),
    Channel("HASH_INT", {'type': 'int'}, int, 0, 'cal_hash'),
    Channel("GDS_HASH_INT", {'type': 'int'}),
]

save_file = ""

def process(ioc, time):
    """
    If either the id or hash has changed, save them.
    :param ioc:
    :param time:
    :return:
    """
    global channels, save_file
    save = False
    for channel in channels:
        new_value = ioc.getParam(channel.name)
        if new_value != channel.value:
            save = True
            channel.value = new_value
    if save:
        try:
            with open(save_file, "wt") as f:
                d = {c.name: c.value for c in channels}
                json.dump(d, f)
        except Exception as e:
          sys.stderr.write(f"Failed to save calibration id and hash: {str(e)}\n")


def build_ioc(ifo: str, savefile: str = "") -> SoftIOC:
    global channels, save_file
    save_file = savefile
    try:
        with open(save_file, "rt") as f:
            saved_vals = json.load(f)
            for channel in channels:
                if channel.name in saved_vals:
                    load_name = channel.name
                else:
                    load_name = channel.get_save_name()
                channel.value = channel.convert_type(saved_vals.get(load_name, channel.value))
    except IOError as e:
        sys.stderr.write(f"Failed to read save file '{save_file}': {str(e)}\n")

    ioc = SoftIOC(
        prefix = f"{ifo}:CAL-CALIB_REPORT_",
        process_period_sec=0.5,
        process_func=process
    )

    chans = {c.name: c.chan_def for c in channels}
    ioc.add_channels(chans)

    ioc.finalize_channels()

    for channel in channels:
        ioc.setParam(channel.name, channel.value)

    return ioc


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='cal_hash_ioc',
        description='Store and serve the calibration ID and hash.'
    )

    parser.add_argument('ifo', help="the two character IFO code, capitalized, e.g. 'X1'")
    parser.add_argument('save_file', default="", nargs="?", help="Location of the file to save current values.")
    args = parser.parse_args()

    ioc = build_ioc(args.ifo, args.save_file)

    ioc.start()
